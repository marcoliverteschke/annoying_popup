(function($, Drupal, drupalSettings) {
    Drupal.behaviors.annoyingPopups = {
      attach(context) {
        $(document).ready(function(){
          if(!drupalSettings.annoyingPopupCookieAware) {
            console.debug('Initialize popups right away.');
            Drupal.behaviors.annoyingPopups.initalizePopups(context);
          } else {
            console.debug('Wait for cookie banner!');
            $('.eu-cookie-compliance-banner .agree-button', context).click(function() {
              console.debug("Just accepted.");
              Drupal.behaviors.annoyingPopups.initalizePopups(context);
            });
            if (Drupal.eu_cookie_compliance.hasAgreed()){
              console.debug("Accepted earlier");
              Drupal.behaviors.annoyingPopups.initalizePopups(context);
            }          
          }
        });
      },
      initalizePopups(context) {
        if(drupalSettings.annoyingPopup.length > 0) {
          let activePopups = 0;
          drupalSettings.annoyingPopup.forEach(annoyingPopup => {
            let popup_id = `annoying_popup-${annoyingPopup.id}`;
            if(Cookies.get(`${popup_id}`) === undefined) {
              activePopups++;
              let popup_actions = '';
              if(annoyingPopup.dismiss_button.title.length > 0) {
                popup_actions += `<li><button class="annoying_popup-button annoying_popup-button-secondary" data-action="dismiss">${annoyingPopup.dismiss_button.title}</button></li>`;
              }
              if(annoyingPopup.action_button.title.length > 0 && annoyingPopup.action_button.url.length > 0) {
                popup_actions += `<li><a href="${annoyingPopup.action_button.url}" class="annoying_popup-button annoying_popup-button-primary" tabindex="0" data-action="engage">${annoyingPopup.action_button.title}</a></li>`;
              }
              let popup_template = `
                <div class="annoying_popup" id="${popup_id}">
                  <div class="annoying_popup-content">
                    ${annoyingPopup.content}
                  </div>
                  <ul class="annoying_popup-actions">${popup_actions}</ul>
                </div>`;
              $('body', context).append(popup_template);
              $(`#${popup_id} [data-action=dismiss]`, context).on('click', function(){
                $(`#${popup_id}`, context).removeAttr('visible');
                // Fixed lifetime of one year for now.
                let cookieMaxAge = 60 * 60 * 24 * 365;
                Cookies.set(`${popup_id}`, 'dismissed-client', { expires: cookieMaxAge, path: '/', secure: true, sameSite: 'strict'});
                if($('.annoying_popup[visible]', context).length === 0) {
                  $('.annoying_popup-overlay', context).removeAttr('visible');
                }
              });
              $(`#${popup_id} [data-action=engage]`, context).on('click', function(event){
                event.preventDefault();
                // Fixed lifetime of one year for now.
                let cookieMaxAge = 60 * 60 * 24 * 365;
                Cookies.set(`${popup_id}`, 'engaged-client', { expires: cookieMaxAge, path: '/', secure: true, sameSite: 'strict'});
                if (this.href) {
                  let target = this.href;
                  window.location = target;
                }
              });
              window.setTimeout(function(){
                $(`#${popup_id}`, context).attr('visible', 'visible');
              }, 1000);
            }
          });

          if(activePopups > 0) {
            let overlay_template = `<div class="annoying_popup-overlay"></div>`;
            $('body', context).append(overlay_template);
            window.setTimeout(function(){
              $('.annoying_popup-overlay', context).attr('visible', 'visible');
            }, 10);
          }
        }
      }
    };
  })(jQuery, Drupal, drupalSettings);
  