<?php

namespace Drupal\annoying_popup\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The AnnoyingPopupRequestSubscriber class.
 */
class AnnoyingPopupRequestSubscriber implements EventSubscriberInterface {

  /**
   * Re-sets cookies server side with a longer lifetime.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The GetResponseEvent.
   */
  public function extendCookieLifetime(GetResponseEvent $event): void {
    foreach ($_COOKIE as $cookieName => $cookieValue) {
      if (strpos($cookieName, 'annoying_popup-') === 0 && preg_match('/(.+)-client$/', $cookieValue, $cookieActualValue) && isset($cookieActualValue[1])) {
        unset($_COOKIE[$cookieName]);
        setcookie($cookieName, $cookieActualValue[1], time() + (60 * 60 * 24 * 365), '/', '', TRUE, FALSE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['extendCookieLifetime'];
    return $events;
  }

}
