<?php

namespace Drupal\annoying_popup\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\annoying_popup\AnnoyingPopupInterface;

/**
 * Defines the AnnoyingPopup entity.
 *
 * @ConfigEntityType(
 *   id = "annoying_popup",
 *   label = @Translation("AnnoyingPopup"),
 *   handlers = {
 *     "list_builder" = "Drupal\annoying_popup\Controller\AnnoyingPopupListBuilder",
 *     "form" = {
 *       "add" = "Drupal\annoying_popup\Form\AnnoyingPopupForm",
 *       "edit" = "Drupal\annoying_popup\Form\AnnoyingPopupForm",
 *       "delete" = "Drupal\annoying_popup\Form\AnnoyingPopupDeleteForm",
 *     }
 *   },
 *   config_prefix = "annoying_popup",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "enabled",
 *     "content",
 *     "action_button",
 *     "dismiss_button",
 *     "visibility"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/annoying_popup/{annoying_popup}",
 *     "delete-form" = "/admin/config/system/annoying_popup/{annoying_popup}/delete",
 *   }
 * )
 */
class AnnoyingPopup extends ConfigEntityBase implements AnnoyingPopupInterface {

  /**
   * The AnnoyingPopup ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The AnnoyingPopup label.
   *
   * @var string
   */
  protected $label;

  /**
   * The AnnoyingPopup enabled status.
   *
   * @var bool
   */
  protected $enabled;

  /**
   * The AnnoyingPopup text content.
   *
   * @var array
   */
  protected $content;

  /**
   * The AnnoyingPopup visibility settings.
   *
   * @var array
   */
  protected $visibility;

  /**
   * The AnnoyingPopup action button setting.
   *
   * @var array
   */
  protected $action_button;

  /**
   * The AnnoyingPopup dismiss button setting.
   *
   * @var array
   */
  protected $dismiss_button;

  /**
   * Get the AnnoyingPopup text content.
   *
   * @return array
   *   The text content.
   */
  public function getContent(): array {
    return $this->content;
  }

  /**
   * Set the AnnoyingPopup text content.
   *
   * @param array $content
   *   The AnnoyingPopup text content.
   *
   * @return self
   *   The entity.
   */
  public function setContent(array $content) {
    $this->content = $content;

    return $this;
  }

  /**
   * Get the AnnoyingPopup enabled status.
   *
   * @return bool
   *   Is the AnnoyingPopup enabled?
   */
  public function getEnabled(): bool {
    return $this->enabled;
  }

  /**
   * Set the AnnoyingPopup enabled status.
   *
   * @param bool $enabled
   *   The AnnoyingPopup enabled status.
   *
   * @return self
   *   The entity.
   */
  public function setEnabled(bool $enabled) {
    $this->enabled = $enabled;

    return $this;
  }

  /**
   * Get the AnnoyingPopup visibility settings.
   *
   * @return array
   *   The visibility settings.
   */
  public function getVisibility() {
    return $this->visibility;
  }

  /**
   * Set the AnnoyingPopup visibility settings.
   *
   * @param array $visibility
   *   The AnnoyingPopup visibility settings.
   *
   * @return self
   *   The entity.
   */
  public function setVisibility(array $visibility) {
    $this->visibility = $visibility;

    return $this;
  }

  /**
   * Get the cache tag for this entity.
   *
   * @return string
   *   The cache tag.
   */
  public function getCacheTag() {
    return 'annoying_popup:' . $this->id;
  }

  /**
   * Get the value of action_button.
   *
   * @return array
   *   Values for the action button.
   */
  public function getActionButton() {
    return $this->action_button;
  }

  /**
   * Set the value of action_button.
   *
   * @return self
   *   The entity.
   */
  public function setActionButton($action_button) {
    $this->action_button = $action_button;

    return $this;
  }

  /**
   * Get the value of dismiss_button.
   *
   * @return array
   *   Values for the dismiss button.
   */
  public function getDismissButton() {
    return $this->dismiss_button;
  }

  /**
   * Set the value of dismiss_button.
   *
   * @return self
   *   The entity.
   */
  public function setDismissButton($dismiss_button) {
    $this->dismiss_button = $dismiss_button;

    return $this;
  }

}
