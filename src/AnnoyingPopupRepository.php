<?php

namespace Drupal\annoying_popup;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\Renderer;
use Drupal\path_alias\AliasManager;

/**
 * The AnnoyingPopupRepository class.
 */
class AnnoyingPopupRepository {

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * The PathMatcher.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  private $pathMatcher;

  /**
   * The CurrentPathStack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private $currentPath;

  /**
   * The AliasManager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  private $aliasManager;

  /**
   * The Renderer.
   *
   * @var \Drupal\Core\Renderer
   */
  private $renderer;

  /**
   * The AnnoyingPopupRepository constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, PathMatcher $pathMatcher, CurrentPathStack $currentPath, AliasManager $aliasManager, Renderer $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pathMatcher = $pathMatcher;
    $this->currentPath = $currentPath;
    $this->aliasManager = $aliasManager;
    $this->renderer = $renderer;
  }

  /**
   * Get popups for the current path.
   *
   * @return array
   *   The popups, if any.
   */
  public function getPopupsForTheCurrentPath() {
    $annoyingPopupStorage = $this->entityTypeManager
      ->getStorage('annoying_popup');
    $annoyingPopupIds = $annoyingPopupStorage
      ->getQuery()
      ->condition('enabled', TRUE)
      ->execute();
    if (!empty($annoyingPopupIds)) {
      $annoyingPopups = $annoyingPopupStorage->loadMultiple($annoyingPopupIds);

      foreach ($annoyingPopups as $key => $annoyingPopup) {
        $currentPath = $this->currentPath->getPath();
        $currentAlias = $this->aliasManager->getAliasByPath($currentPath);
        $isPathMatch = $this->pathMatcher
          ->matchPath($currentAlias, $annoyingPopup->get('visibility')['request_path']['pages']);
        $negateMatch = (bool) $annoyingPopup->get('visibility')['request_path']['negate'];
        if (($negateMatch === TRUE && $isPathMatch === TRUE) || ($negateMatch === FALSE && $isPathMatch === FALSE)) {
          unset($annoyingPopups[$key]);
        }
      }

      return $annoyingPopups;
    }
    return [];
  }

  /**
   * Are there popups to be show on the current path?
   *
   * @return bool
   *   Well, are there?
   */
  public function hasPopupsForCurrentPath() {
    $annoyingPopups = $this->getPopupsForTheCurrentPath();
    return count($annoyingPopups) > 0;
  }

  /**
   * Compile values for JavaScript settings.
   *
   * @return array
   *   The settings.
   */
  public function getPopupsForCurrentPathJavaScriptSettings() {
    $annoyingPopups = $this->getPopupsForTheCurrentPath();
    $settings = [];
    foreach ($annoyingPopups as $annoyingPopup) {
      $contentBuild = [
        '#type' => 'processed_text',
        '#text' => $annoyingPopup->getContent()['value'],
        '#format' => $annoyingPopup->getContent()['format'],
      ];
      $settings[] = [
        'id' => $annoyingPopup->id(),
        'content' => (string) $this->renderer->renderPlain($contentBuild),
        'action_button' => [
          'url' => $annoyingPopup->getActionButton()['url'],
          'title' => $annoyingPopup->getActionButton()['title'],
        ],
        'dismiss_button' => [
          'title' => $annoyingPopup->getDismissButton()['title'],
        ],
      ];
    }
    return $settings;
  }

  /**
   * The cache tags for all popups.
   *
   * @return array
   *   The cache tag strings.
   */
  public function getCacheTags() {
    $annoyingPopups = $this->getPopupsForTheCurrentPath();
    $cacheTags = [];
    foreach ($annoyingPopups as $annoyingPopup) {
      $cacheTags[] = $annoyingPopup->getCacheTag();
    }
    return $cacheTags;
  }

}
